const HtmlWebpackPlugin = require("html-webpack-plugin")
const path = require("path")

const rulesCSS = {
  test: /.(css|sass|scss)$/,
  use: ["style-loader", "css-loader", "sass-loader"],
}

const rulesJs = {
  test: /\.(js|ts)x?$/,
  exclude: /node_modules/,
  loader: "babel-loader",
  options: {
    presets: [
      [
        "@babel/preset-react",
        {
          runtime: "automatic",
        },
      ],
      "@babel/preset-typescript",
    ],
  },
}

const rulesAsset = {
  type: "asset",
  test: /\.(png|svg|jpg|jpeg|gif)$/i,
}

module.exports = (env, argv) => {
  const { mode } = argv
  const isProduction = mode === "production"
  return {
    entry: "./src/index.tsx",
    output: {
      filename: isProduction ? "[name].[contenthash].js" : "main.js",
      path: path.resolve(__dirname, "build"),
    },
    plugins: [new HtmlWebpackPlugin({ template: "public/index.html" })],
    resolve: {
      extensions: [".ts", ".tsx", ".js", ".jsx"],
      alias: {
        components: path.resolve(__dirname, "src/components/"),
        views: path.resolve(__dirname, "src/views"),
        assets: path.resolve(__dirname, "public/assets"),
        utils: path.resolve(__dirname, "src/utils"),
        models: path.resolve(__dirname, "src/models"),
        hooks: path.resolve(__dirname, "src/hooks"),
        api: path.resolve(__dirname, "src/api"),
        context: path.resolve(__dirname, "src/context"),
      },
    },
    module: {
      rules: [rulesJs, rulesCSS, rulesAsset],
    },
    devServer: {
      static: {
        directory: path.join(__dirname, "public"),
      },
      compress: true,
      open: true,
      port: 3001,
    },
    devtool: "source-map",
  }
}
