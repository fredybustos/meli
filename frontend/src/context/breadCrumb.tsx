import React, { useState, createContext } from "react"

export const Context = createContext<{
  crumbs: CrumbsProps[]
  addCrumbs: (arg: CrumbsProps[]) => void
}>({
  crumbs: [],
  addCrumbs: () => {},
})

export const BreadCrumbProvider = ({
  children,
}: {
  children: React.ReactNode
}) => {
  const [crumbs, setCrumbs] = useState<CrumbsProps[]>([])

  function addCrumbs(currentCrumbs: CrumbsProps[]) {
    setCrumbs(currentCrumbs)
  }

  return (
    <Context.Provider value={{ crumbs, addCrumbs }}>
      {children}
    </Context.Provider>
  )
}

export default Context

type CrumbsProps = {
  route: string
  title: string
}
