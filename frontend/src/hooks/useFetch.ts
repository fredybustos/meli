import { useState } from "react"
import { AxiosResponse } from "axios"

export default function useFetch() {
  const [data, setSearchResults] = useState<any | null>(null)
  const [loading, setLoading] = useState(false)

  const addResults = (data: any) => setSearchResults(data)

  const getData = ({
    service,
    params,
  }: {
    service: (arg: Params) => Promise<AxiosResponse<any>>
    params: Params
  }) => {
    setLoading(true)
    service(params)
      .then(({ data }) => {
        addResults(data)
      })
      .catch((err: any) => {
        console.error(err, "No se econtraron resultados relacionados")
      })
      .finally(() => {
        setLoading(false)
      })
  }

  return { getData, data, loading }
}

type Params = {
  [k: string]: string | number | any
}
