// @ts-nocheck
import { renderHook, cleanup, act } from "@testing-library/react-hooks"

import axiosMock from "axios"
import useFetch from "../useFetch"
import { GetProducts } from "api"

describe("UseFetch", () => {
  beforeEach(() => {
    cleanup()
    jest.clearAllMocks()
  })

  test("Should get data", async () => {
    const mockResponse = [{ id: "MLA910190287", title: "iphone" }]
    axiosMock.get.mockImplementation(() =>
      Promise.resolve({
        data: mockResponse,
      })
    )
    const { result, waitForNextUpdate } = renderHook(() => useFetch())

    expect(result.current.data).toEqual(null)

    act(() => {
      result.current.getData({
        service: GetProducts,
        params: { search: "iphone" },
      })
    })

    await waitForNextUpdate()

    expect(result.current.loading).toBeFalsy()
    expect(result.current.data).toEqual(mockResponse)
  })
})
