import { formatCurrency, parseQuery, getCondition } from "../index"

describe("Utils", () => {
  test("Format currency", () => {
    const currency = formatCurrency("284999")
    expect(formatCurrency("284999")).toEqual(currency)
    expect(formatCurrency("284999", "es-CO")).not.toBe(currency)
    expect(formatCurrency("")).toEqual("-")
  })
  test("Parse query", () => {
    const params = parseQuery("?search=iphone")
    expect(parseQuery("?search=text")).not.toBe(params)
  })
  test("Get condition", () => {
    expect(getCondition("new")).toBe("Nuevo")
    expect(getCondition("old")).toBe("Usado")
    expect(getCondition("otro")).toBe("-")
  })
})
