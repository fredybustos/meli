import queryString from "query-string"

export const parseQuery = (params: string) => queryString.parse(params)

export const formatCurrency = (amount: number | string, locale?: string) => {
  if (amount) {
    const formatter = new Intl.NumberFormat(locale || "es-AR", {
      style: "currency",
      currency: "ARS",
    })
    return formatter.format(Number(amount))
  }
  return "-"
}

const conditions = {
  new: "Nuevo",
  old: "Usado",
}

export const getCondition = (condition: string) => {
  if (!conditions[condition]) return "-"
  return conditions[condition]
}
