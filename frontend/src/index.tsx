import ReactDOM from 'react-dom'
import App from './app'
import './css/main.scss'

ReactDOM.render(
  <App />,
  document.getElementById('root')
)
