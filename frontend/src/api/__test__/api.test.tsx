// @ts-nocheck
import axiosMock from "axios"

import { GetProducts, GetProductDetail } from "api"
import { PRODUCTS, PRODUCT } from "views/__fixtures__"

const API_URL = "http://localhost:3000/api"
const PRODUCT_ID = "MLA935267786"

jest.mock("axios")

describe("Requests", () => {
  test("should return products", async () => {
    axiosMock.get.mockResolvedValueOnce(PRODUCTS)

    const result = await GetProducts({ search: "iphone" })

    expect(axiosMock.get).toHaveBeenCalledWith(`${API_URL}/items`, {
      params: {
        search: "iphone",
      },
    })
    expect(result).toEqual(PRODUCTS)
  })
  test("should return product", async () => {
    axiosMock.get.mockResolvedValueOnce(PRODUCT)

    const result = await GetProductDetail({ id: PRODUCT_ID })

    expect(axiosMock.get).toHaveBeenCalledWith(`${API_URL}/items/${PRODUCT_ID}`)

    expect(result).toEqual(PRODUCT)
  })

  test("Should return empty products", async () => {
    axiosMock.get.mockImplementation(() => Promise.resolve([]))

    const result = await GetProducts({ search: "12345" })

    expect(axiosMock.get).toHaveBeenCalledWith(`${API_URL}/items`, {
      params: {
        search: "12345",
      },
    })
    expect(result).toEqual([])
  })
  test("Should return empty product", async () => {
    axiosMock.get.mockImplementation(() => Promise.resolve([]))

    const result = await GetProductDetail({ id: "uyrbj7667" })

    expect(axiosMock.get).toHaveBeenCalledWith(`${API_URL}/items/uyrbj7667`)

    expect(result).toEqual([])
  })
})
