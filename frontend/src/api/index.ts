import axios from "axios"

const API_URL = "https://apply-meli.herokuapp.com/api"

export const GetProducts = (params: { [k: string]: string }) => {
  return axios.get(`${API_URL}/items`, { params })
}

export const GetProductDetail = (params: { [k: string]: string }) => {
  return axios.get(`${API_URL}/items/${params.id}`)
}
