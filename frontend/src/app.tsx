import { lazy, Suspense } from "react"
import { BrowserRouter as Router, Switch, Route } from "react-router-dom"
import { ErrorBoundary } from "react-error-boundary"
import Spinner from "components/spinner"
import Error from "components/error"
import { BreadCrumbProvider } from "context/breadCrumb"

const Products = lazy(() => import("views/products"))
const ProductDetail = lazy(() => import("views/productDetail"))
const Layout = lazy(() => import("views/layout"))

function App() {
  return (
    <BreadCrumbProvider>
      <Router>
        <Suspense fallback={<Spinner />}>
          <ErrorBoundary FallbackComponent={Error}>
            <Layout>
              <Switch>
                <Route exact path="/" />
                <Route exact path="/items" component={Products} />
                <Route exact path="/items/:id" component={ProductDetail} />
              </Switch>
            </Layout>
          </ErrorBoundary>
        </Suspense>
      </Router>
    </BreadCrumbProvider>
  )
}

export default App
