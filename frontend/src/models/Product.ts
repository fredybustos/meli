export interface Product {
  id?: string
  title: string
  price: Price
  picture: string
  condition: string
  freeShipping: boolean
  soldQuantity: number
  description?: string
  city: string
}

type Price = {
  amount: number
  currency: string
  decimals: number
}
