import React, { CSSProperties } from "react"
import "./button.scss"

export default function Button({
  onClick,
  variant,
  style,
  className,
  children,
  testId,
}: ButtonProps) {
  return (
    <button
      onClick={onClick}
      className={`button ${variant} ${className || ""}`}
      style={style}
      data-testid={`button-${testId}`}
    >
      {variant === "iconSearch" ? (
        <figure>
          <img src="/assets/ic_Search.png" alt="icon-search" />
        </figure>
      ) : (
        <span>{children}</span>
      )}
    </button>
  )
}

type ButtonProps = {
  onClick?: () => void
  variant: "primary" | "iconSearch"
  style?: CSSProperties
  className?: string
  testId?: string
  children?: React.ReactNode
}
