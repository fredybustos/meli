import { useHistory } from "react-router-dom"
import "./breadCrumb.scss"

export default function Breadcrumb({ crumbs }) {
  const history = useHistory()
  const hanldeRoute = (route: string) => {
    history.push(route)
  }

  return (
    <nav className="breadcrumb">
      <ul className="breadcrumb__list">
        {crumbs.map(({ route, title }, index) => {
          return (
            <li key={index} className="breadcrumb__item">
              <button
                data-testid={`breadcrumb-${index}`}
                className="breadcrumb__button"
                onClick={() => hanldeRoute(route)}
              >
                {title}
              </button>
            </li>
          )
        })}
      </ul>
    </nav>
  )
}
