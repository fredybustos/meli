import "./spinner.scss"

export default function Spinner() {
  return <div className="spinner" aria-label="spinner" data-testid="spinner" />
}
