import Product from "components/product"
import SEO from "components/seo"
import { Product as ProductProps } from "models/Product"
import "./product.scss"

export default function ({
  list,
  search,
}: {
  list: ProductProps[]
  search: string | string[] | null
}) {
  return (
    <ul className="listProduct" data-testid="listProduct">
      <SEO title={`Mercado libre ${search}`} />
      {list.length === 0 ? (
        <li className="listProduct__result">
          No se encontraron resultados para: {search}
        </li>
      ) : (
        <>
          {list.map(
            ({
              id,
              title,
              price,
              picture,
              condition,
              freeShipping,
              soldQuantity,
              description,
              city,
            }) => (
              <Product
                key={id}
                id={id}
                title={title}
                price={price}
                picture={picture}
                condition={condition}
                soldQuantity={soldQuantity}
                freeShipping={freeShipping}
                description={description}
                city={city}
              />
            )
          )}
        </>
      )}
    </ul>
  )
}
