import { render } from "@testing-library/react"

import Spinner from "components/spinner"

describe("Spinner component", () => {
  test("Should render spinner", () => {
    const { getByTestId } = render(<Spinner />)
    expect(getByTestId("spinner")).toBeTruthy()
  })
})
