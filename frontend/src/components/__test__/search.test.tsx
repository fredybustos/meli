import { render, fireEvent } from "@testing-library/react"
import { MemoryRouter } from "react-router"

import Search from "components/search"

const mockHistoryPush = jest.fn()

jest.mock("react-router-dom", () => ({
  ...jest.requireActual("react-router-dom"),
  useHistory: () => ({
    push: mockHistoryPush,
  }),
}))

describe("Search component", () => {
  test("Should render search results", () => {
    const { getByTestId, getByAltText } = render(
      <MemoryRouter>
        <Search />
      </MemoryRouter>
    )

    expect(getByTestId("search")).toBeTruthy()
    expect(getByAltText("Mercado Libre")).toBeTruthy()

    const input = getByTestId("search-input")
    expect(input).toBeTruthy()

    fireEvent.change(input, { target: { value: "iphone" } })

    expect(getByAltText("icon-search")).toBeTruthy()

    const button = getByTestId("button-search")
    expect(button).toBeTruthy()

    fireEvent.click(button)
    expect(mockHistoryPush).toHaveBeenCalledWith({
      pathname: "/items",
      search: "?search=iphone",
    })
  })
})
