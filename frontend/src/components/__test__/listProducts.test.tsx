import { render } from "@testing-library/react"

import ListProduct from "components/listProduct"
import { formatCurrency } from "utils"

describe("ListProducts component", () => {
  test("Should render empty message", () => {
    const props = {
      list: [],
      search: "1234",
    }
    const { getByTestId } = render(<ListProduct {...props} />)

    expect(getByTestId("listProduct")).toBeTruthy()

    const textMatch = "No se encontraron resultados para: 1234"
    const hasText = (node: Element) =>
      node.textContent === textMatch || node?.textContent?.match(textMatch)

    expect(hasText).toBeTruthy()
  })
  test("Should reder error boundary", () => {
    const props = {
      list: [
        {
          id: "MLA935267786",
          title: "Apple iPhone 12 (128 Gb) - Negro",
          price: {
            amount: 219999,
            currency: "ARS",
            decimals: 0,
          },
          picture:
            "http://http2.mlstatic.com/D_743195-MLA45719932493_042021-I.jpg",
          condition: "new",
          freeShipping: true,
          soldQuantity: 0,
          description: "El mas vendido",
          city: "Capital Federal",
        },
      ],
      search: "iphone",
    }
    const product = props.list[0]
    const { getByTestId, getByText } = render(<ListProduct {...props} />)

    expect(getByTestId("listProduct")).toBeTruthy()

    expect(getByText(product.title)).toBeTruthy()
    expect(getByText(product.city)).toBeTruthy()

    const amount = formatCurrency(product.price.amount)
    expect(amount).toBeTruthy()
  })
})
