import { render } from "@testing-library/react"

import Product from "components/product"
import { formatCurrency } from "utils"

describe("Product component", () => {
  test("Should reder product", () => {
    const props = {
      id: "MLA935267786",
      title: "Apple iPhone 12 (128 Gb) - Negro",
      price: {
        amount: 219999,
        currency: "ARS",
        decimals: 0,
      },
      picture: "http://http2.mlstatic.com/D_743195-MLA45719932493_042021-I.jpg",
      condition: "new",
      freeShipping: true,
      soldQuantity: 0,
      city: "Capital Federal",
    }

    const { getByTestId, getByText } = render(<Product {...props} />)

    expect(getByTestId(`product-item-${props.id}`)).toBeTruthy()

    expect(getByText(props.title)).toBeTruthy()
    expect(getByText(props.city)).toBeTruthy()

    const amount = formatCurrency(props.price.amount)
    expect(amount).toBeTruthy()
  })
})
