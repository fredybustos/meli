import { render, fireEvent } from "@testing-library/react"

import Error from "components/error"

const props = {
  error: {
    message: "Ocurrió un error",
  },
}

const onClickMock = jest.fn()

describe("Error component", () => {
  test("Should reder error boundary", () => {
    const { getByText, getByTestId } = render(
      <Error resetErrorBoundary={onClickMock} {...props} />
    )

    expect(getByText("Algo salio mal:")).toBeTruthy()
    expect(getByText("Ocurrió un error")).toBeTruthy()

    fireEvent.click(getByTestId("button-error"))
    expect(onClickMock).toHaveBeenCalledTimes(1)
  })
})
