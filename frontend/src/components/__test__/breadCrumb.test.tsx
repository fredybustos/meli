import { render, fireEvent } from "@testing-library/react"

import BreadCrumb from "components/breadCrumb"

const mockHistoryPush = jest.fn()

jest.mock("react-router-dom", () => ({
  ...jest.requireActual("react-router-dom"),
  useHistory: () => ({
    push: mockHistoryPush,
  }),
}))

describe("Breadcrumb component", () => {
  const crumbs = [
    { route: "/", title: "Celulares y Smartphones" },
    { route: "/", title: "iphone" },
  ]

  test("Should render breadcrumb", () => {
    const { getByTestId, getByText, debug } = render(
      <BreadCrumb crumbs={crumbs} />
    )

    expect(getByText("Celulares y Smartphones")).toBeTruthy()
    expect(getByText("iphone")).toBeTruthy()

    const breadcrumb = getByTestId("breadcrumb-1")
    expect(breadcrumb).toBeTruthy()

    fireEvent.click(breadcrumb)
    expect(mockHistoryPush).toHaveBeenCalledWith("/")
    debug()
  })
})
