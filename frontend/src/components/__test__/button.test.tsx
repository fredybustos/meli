import { render, fireEvent } from "@testing-library/react"

import Button from "components/button"

const props = {
  style: {},
  className: "",
  testId: "test",
}

const onClickMock = jest.fn()

describe("Button component", () => {
  test("Should render child component", () => {
    const { getByText, getByTestId } = render(
      <Button variant="primary" onClick={onClickMock} {...props}>
        {"child component"}
      </Button>
    )

    expect(getByText("child component")).toBeTruthy()
    fireEvent.click(getByTestId("button-test"))
    expect(onClickMock).toHaveBeenCalledTimes(1)
  })
})
