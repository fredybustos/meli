import Button from "components/button"
import "./error.scss"

export default function ErrorFallback({
  error,
  resetErrorBoundary,
}: {
  error: { message: string }
  resetErrorBoundary: () => void
}) {
  return (
    <div role="alert" className="error">
      <p>Algo salio mal:</p>
      <pre>{error.message}</pre>
      <Button testId="error" variant="primary" onClick={resetErrorBoundary}>
        Regresar
      </Button>
    </div>
  )
}
