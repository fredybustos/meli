import { useState } from "react"
import { useHistory, Link } from "react-router-dom"

import Button from "components/button"
import SEO from "components/seo"
import "./search.scss"

export default function Search() {
  const [searchProduct, setSearchProduct] = useState<string>("")
  const history = useHistory()

  const handleChangeValue = (event: React.ChangeEvent<HTMLInputElement>) => {
    const { value } = event.currentTarget
    setSearchProduct(value)
  }

  const handleSearch = () => {
    history.push({
      pathname: "/items",
      search: `?search=${searchProduct}`,
    })
  }

  const handleSearchProduct = (
    event: React.KeyboardEvent<HTMLInputElement>
  ) => {
    if (event.keyCode === 13) {
      handleSearch()
    }
  }

  return (
    <div className="search" data-testid="search">
      <SEO title="Productos" />
      <div className="search__container">
        <Link to="/">
          <figure>
            <img src="/assets/Logo_ML.png" alt="Mercado Libre" />
          </figure>
        </Link>
        <div className="search__input">
          <input
            id="search"
            name="search"
            data-testid="search-input"
            placeholder="Nunca dejes de buscar"
            value={searchProduct}
            onChange={handleChangeValue}
            onKeyDown={handleSearchProduct}
          />
          <Button testId="search" onClick={handleSearch} variant="iconSearch" />
        </div>
      </div>
    </div>
  )
}
