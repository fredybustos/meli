import { useHistory } from "react-router-dom"

import { Product as ProductProps } from "models/Product"
import { formatCurrency } from "utils"
import "./product.scss"

export default function Product({
  id,
  title,
  price,
  picture,
  freeShipping,
  city,
}: ProductProps) {
  const history = useHistory()
  const handleProduct = () => history.push(`/items/${id}`)

  const handleSearchProduct = (event: React.KeyboardEvent<any>) => {
    if (event.keyCode === 13) {
      handleProduct()
    }
  }

  return (
    <li
      className="product"
      tabIndex={0}
      role="button"
      data-testid={`product-item-${id}`}
      onClick={handleProduct}
      onKeyDown={handleSearchProduct}
    >
      <figure className="product__image">
        <img src={picture} alt={title} />
      </figure>
      <div className="product__info">
        <div className="product__info__ctntext">
          <p>{formatCurrency(price?.amount)}</p>
          {freeShipping && (
            <img src="/assets/ic_shipping.png" alt="Envio gratis" />
          )}
        </div>
        <p className="product__info__text">{title}</p>
      </div>
      <p className="product__city">{city}</p>
    </li>
  )
}
