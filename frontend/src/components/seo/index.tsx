import Helmet from "react-helmet"

export default function Seo({ title }) {
  return (
    <Helmet defaultTitle="Mercdo Libre">
      <meta name="description" content={title}></meta>
      <title>{title}</title>
    </Helmet>
  )
}
