import { useContext } from "react"
import { useLocation } from "react-router-dom"

import Context from "context/breadCrumb"
import Search from "components/search"
import Breadcrumb from "components/breadCrumb"
import "./layout.scss"

const PATHNAME = "/"

export default function Layout({ children }: { children: React.ReactNode }) {
  const location = useLocation()
  const { crumbs } = useContext(Context)
  return (
    <div className="layout">
      <Search />
      <div data-testid="results">
        {location.pathname !== PATHNAME && <Breadcrumb crumbs={crumbs} />}
        <div className="layout__children">{children}</div>
      </div>
    </div>
  )
}
