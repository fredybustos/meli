import { useEffect, useContext } from "react"
import { useParams } from "react-router-dom"

import Context from "context/breadCrumb"
import useFetch from "hooks/useFetch"
import Spinner from "components/spinner"
import Button from "components/button"
import SEO from "components/seo"
import { Product as ProductProps } from "models/Product"
import { formatCurrency, getCondition } from "utils"
import { GetProductDetail } from "api"
import "./productDetail.scss"

export default function ProductDetail() {
  const params = useParams<{ id: string }>()
  const { addCrumbs } = useContext(Context)
  const { getData, data, loading } = useFetch()

  useEffect(() => {
    getData({ service: GetProductDetail, params: { id: params.id } })
  }, [params.id])

  useEffect(() => {
    if (data) {
      addCrumbs([{ route: "/", title: product.title }])
    }
  }, [data])

  const product = data || ({} as ProductProps)

  return loading ? (
    <div className="grid-center">
      <SEO title="Cargando" />
      <Spinner />
    </div>
  ) : (
    <div className="productDetail" data-testid="productDetail">
      <SEO title={product.title} />
      <figure>
        <img src={product.picture} alt={product.title} />
      </figure>
      <div className="productDetail__info">
        <p className="productDetail__info__conditionText">
          {getCondition(product.condition)}-{product.soldQuantity} vendidos
        </p>
        <p className="productDetail__info__title">{product.title}</p>
        <p className="productDetail__info__cost">
          {product.price?.amount && formatCurrency(product.price?.amount)}
        </p>
        <Button testId="buy" variant="primary" onClick={() => {}}>
          Comprar
        </Button>
      </div>
      <div className="productDetail__ctndescription">
        <h2>Descripción del producto</h2>
        <div className="productDetail__ctndescription__description">
          {product.description}
        </div>
      </div>
    </div>
  )
}
