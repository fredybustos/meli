export const PRODUCTS = {
  author: {
    name: "Fredy",
    lastname: "Bustos",
  },
  categories: [
    {
      id: "MLA1055",
      name: "Celulares y Smartphones",
      path_from_root: [
        {
          id: "MLA1051",
          name: "Celulares y Teléfonos",
        },
        {
          id: "MLA1055",
          name: "Celulares y Smartphones",
        },
      ],
    },
  ],
  items: [
    {
      id: "MLA935267786",
      title: "Apple iPhone 12 (128 Gb) - Negro",
      price: {
        amount: 219999,
        currency: "ARS",
        decimals: 0,
      },
      picture: "http://http2.mlstatic.com/D_743195-MLA45719932493_042021-I.jpg",
      condition: "new",
      freeShipping: true,
      city: "Capital Federal",
    },
    {
      id: "MLA910190287",
      title: "Apple iPhone 11 (128 Gb) - Negro",
      price: {
        amount: 199700,
        currency: "ARS",
        decimals: 0,
      },
      picture: "http://http2.mlstatic.com/D_865864-MLA46114990464_052021-I.jpg",
      condition: "new",
      freeShipping: true,
      city: "Buenos Aires",
    },
    {
      id: "MLA886428988",
      title: "Apple iPhone SE (2da Generación) 64 Gb - Negro",
      price: {
        amount: 112100,
        currency: "ARS",
        decimals: 0,
      },
      picture: "http://http2.mlstatic.com/D_658260-MLA46552695787_062021-I.jpg",
      condition: "new",
      freeShipping: true,
      city: "Buenos Aires",
    },
    {
      id: "MLA935783994",
      title: "Apple iPhone 11 (64 Gb) - Blanco",
      price: {
        amount: 164000,
        currency: "ARS",
        decimals: 0,
      },
      picture: "http://http2.mlstatic.com/D_809326-MLA46115014340_052021-I.jpg",
      condition: "new",
      freeShipping: true,
      city: "Capital Federal",
    },
  ],
}

export const PRODUCT = {
  title: "Apple iPhone 12 (128 Gb) - Negro",
  item: {
    id: "MLA935267786",
  },
  price: {
    amount: 219999,
    currency: "ARS",
    decimals: 0,
  },
  picture: "http://http2.mlstatic.com/D_743195-MLA45719932493_042021-I.jpg",
  condition: "new",
  soldQuantity: 1,
  city: "Capital Federal",
  description:
    "El iPhone 12 tiene una espectacular pantalla Super Retina XDR de 6.1 pulgadas (1). Un frente de Ceramic Shield, cuatro veces más resistente a las caídas (2). Modo Noche en todas las cámaras, para que puedas tomar fotos increíbles con poca luz. Grabación, edición y reproducción de video en Dolby Vision con calidad cinematográfica. Y el potente chip A14 Bionic. Además, es compatible con los nuevos accesorios MagSafe, que se acoplan fácilmente a tu iPhone y permiten una carga inalámbrica más rápida (3). Que comience la diversión.\n\n\nAvisos Legales\n(1) La pantalla tiene las esquinas redondeadas. Si se mide en forma de rectángulo, la pantalla tiene 6.06 pulgadas en diagonal. El área real de visualización es menor.\n(2) La afirmación se basa en la comparación de la parte frontal de Ceramic Shield del iPhone 12 con respecto a la generación anterior de iPhone.\n(3) Los accesorios se venden por separado.\n(4) El iPhone 12 es resistente a las salpicaduras, al agua y al polvo, y fue probado en condiciones de laboratorio controladas, con una clasificación IP68 según la norma IEC 60529 (hasta 30 minutos a una profundidad máxima de 6 metros). La resistencia a las salpicaduras, al agua y al polvo no es una condición permanente y podría disminuir como consecuencia del uso normal. No intentes cargar un iPhone mojado; consulta el manual del usuario para ver las instrucciones de limpieza y secado. La garantía no cubre daños producidos por líquidos.",
  category_id: "MLA1055",
}
