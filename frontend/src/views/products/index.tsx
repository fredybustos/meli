import { useEffect, useContext } from "react"
import { useLocation } from "react-router-dom"

import Context from "context/breadCrumb"
import useFetch from "hooks/useFetch"
import ListProduct from "components/listProduct"
import Spinner from "components/spinner"
import SEO from "components/seo"
import { Product as ProductProps } from "models/Product"
import { GetProducts } from "api"
import { parseQuery } from "utils"

export default function index() {
  const location = useLocation()
  const { addCrumbs } = useContext(Context)
  const { search } = parseQuery(location.search)
  const { getData, data, loading } = useFetch()

  useEffect(() => {
    getData({ service: GetProducts, params: { search } })
  }, [search])

  useEffect(() => {
    if (data) {
      const categoryName = data.categories.map((item) => item.name)
      const crumbs = [
        { route: "/", title: categoryName[0] },
        { route: "/", title: search },
      ]
      addCrumbs(crumbs)
    }
  }, [data])

  const list = data?.items || ([] as ProductProps[])

  return loading ? (
    <div className="grid-center">
      <SEO title="Cargando productos" />
      <Spinner />
    </div>
  ) : (
    <ListProduct list={list} search={search} />
  )
}
