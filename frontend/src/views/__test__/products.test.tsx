// @ts-nocheck
import { render, waitFor, cleanup, fireEvent } from "@testing-library/react"
import "@testing-library/jest-dom"
import axiosMock from "axios"

import Products from "../products"
import { formatCurrency } from "utils"
import { BreadCrumbProvider } from "context/breadCrumb"
import { PRODUCTS } from "../__fixtures__"

const API_URL = "http://localhost:3000/api"
const mockHistoryPush = jest.fn()

jest.mock("axios")

jest.mock("react-router-dom", () => ({
  ...jest.requireActual("react-router-dom"),
  useLocation: () => ({
    search: "?search=iphone",
  }),
  useHistory: () => ({
    push: mockHistoryPush,
  }),
}))

describe("Products", () => {
  beforeEach(() => {
    cleanup()
    jest.clearAllMocks()
  })

  test("Should render products to search", async () => {
    axiosMock.get.mockImplementation(() =>
      Promise.resolve({
        data: PRODUCTS,
      })
    )

    const product = PRODUCTS.items[0]

    const { getByTestId, getAllByText } = render(
      <BreadCrumbProvider>
        <Products />
      </BreadCrumbProvider>
    )

    expect(getByTestId("spinner")).toBeTruthy()
    expect(axiosMock.get).toHaveBeenCalledTimes(1)
    expect(axiosMock.get).toHaveBeenCalledWith(`${API_URL}/items`, {
      params: {
        search: "iphone",
      },
    })

    await waitFor(() => getByTestId("listProduct"))

    expect(getAllByText(product.title)).toBeTruthy()
    expect(getAllByText(product.city)).toBeTruthy()

    const amount = formatCurrency(product.price.amount)
    expect(amount).toBeTruthy()

    const productId = getByTestId(`product-item-${product.id}`)
    expect(productId).toBeTruthy()

    fireEvent.click(productId)
    expect(mockHistoryPush).toHaveBeenCalledWith(`/items/${product.id}`)
  })
})
