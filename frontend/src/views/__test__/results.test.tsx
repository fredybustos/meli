// @ts-nocheck
import React from "react"
import { render, waitFor, cleanup } from "@testing-library/react"
import "@testing-library/jest-dom"
import axiosMock from "axios"

import Products from "../products"
import { BreadCrumbProvider } from "context/breadCrumb"

const API_URL = "http://localhost:3000/api"
const mockHistoryPush = jest.fn()

jest.mock("axios")

jest.mock("react-router-dom", () => ({
  ...jest.requireActual("react-router-dom"),
  useLocation: () => ({
    search: "?search=1234",
  }),
  useHistory: () => ({
    push: mockHistoryPush,
  }),
}))

describe("Products", () => {
  const setState = jest.fn()

  const useStateMock = (initState: any) => [initState, setState]

  beforeEach(() => {
    cleanup()
    jest.clearAllMocks()
  })
  test("Should render without results", async () => {
    jest.spyOn(React, "useState").mockImplementation(useStateMock)
    axiosMock.get.mockImplementation(() =>
      Promise.resolve({
        error: "",
      })
    )

    const { getByTestId } = render(
      <BreadCrumbProvider>
        <Products />
      </BreadCrumbProvider>
    )

    expect(axiosMock.get).toHaveBeenCalledTimes(1)
    expect(axiosMock.get).toHaveBeenCalledWith(`${API_URL}/items`, {
      params: {
        search: "1234",
      },
    })

    expect(setState).toHaveBeenCalledTimes(1)

    await waitFor(() => getByTestId("listProduct"))

    const textMatch = "No se encontraron resultados para: 1234"
    const hasText = (node: Element) =>
      node.textContent === textMatch || node.textContent.match(textMatch)

    expect(hasText).toBeTruthy()
  })
})
