// @ts-nocheck
import { render, waitFor, cleanup, fireEvent } from "@testing-library/react"
import "@testing-library/jest-dom"
import axiosMock from "axios"

import ProductDetail from "../productDetail"
import { formatCurrency, getCondition } from "utils"
import { BreadCrumbProvider } from "context/breadCrumb"
import { PRODUCT } from "../__fixtures__"

const API_URL = "http://localhost:3000/api"
const PRODUCT_ID = "MLA935267786"

jest.mock("axios")

jest.mock("react-router-dom", () => ({
  ...jest.requireActual("react-router-dom"),
  useParams: () => ({
    id: PRODUCT_ID,
  }),
}))

describe("Products", () => {
  beforeEach(() => {
    cleanup()
    jest.clearAllMocks()
  })

  test("Should render products to search", async () => {
    axiosMock.get.mockImplementation(() =>
      Promise.resolve({
        data: PRODUCT,
      })
    )

    const { getByTestId, getByText } = render(
      <BreadCrumbProvider>
        <ProductDetail />
      </BreadCrumbProvider>
    )

    expect(getByTestId("spinner")).toBeTruthy()
    expect(axiosMock.get).toHaveBeenCalledTimes(1)
    expect(axiosMock.get).toHaveBeenCalledWith(`${API_URL}/items/${PRODUCT_ID}`)

    await waitFor(() => getByTestId("productDetail"))

    expect(getByText(PRODUCT.title)).toBeTruthy()

    const amount = formatCurrency(PRODUCT.price.amount)
    expect(amount).toBeTruthy()

    const condition = `${getCondition(PRODUCT.condition)}-${
      PRODUCT.soldQuantity
    } Vendidos`

    expect(condition).toBeTruthy()

    const button = getByTestId("button-buy")
    expect(button).toBeTruthy()
  })
})
