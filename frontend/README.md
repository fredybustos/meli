### Mercado Libre

Se crearon 3 vistas buscador, productos y detalle del producto. Al buscar un producto se va a mostrar un `spinner` en espera de la respuesta del API, posteriormente se muestra la lista de productos, desde allí podemos ir al detalle del producto. Se agregó `helmet` para tener SEO(Preferible SSR). Breadcrumb con las categorías del los productos

#### Desarrollo

- [webpack](https://webpack.js.org/) Para manejar el bundle del proyecto. Consultar para más ajustes

- `npm install` Instalar dependencias necesarias para que corra el proyecto

- `npm run dev` Levanta el proyecto en el puerto `3001`
- `npm run test` Corre el set de test del proyecto
- `npm run build` Construye la aplicación para ser desplegada en producción

- El proyecto se puede visualizar en este [link](https://apply-meli.netlify.app/).
