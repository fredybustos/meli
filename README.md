### Mercado Libre

Esta es una prueba para aplicar a Mercado Libre.
La prueba esta dividida en dos proyectos, `backend` y `frontend`.

Cada proyecto tiene su readme para poder ejecutar cada proyecto y validar los requerimientos

#### Backend

Se consulta el API de Mercado Libre para traer y construir la información que será mostrada en el frontend, se consultan los endpoints requeridos y se regresa tal cual el requerimiento.
Esta deplegada en [heroku](https://dashboard.heroku.com/apps)

- [API](https://apply-meli.herokuapp.com/api)

#### Frontend

Se consulta el servicio creado anteriormente, se crea el buscador y las vistas requeridas, se valida la información y se hacen test de integración. Esta desplegada en [netlify](https://www.netlify.com/)

- [App](https://apply-meli.netlify.app/)

**Correr el proyecto**

- Como primer hay que levantar el `backend` que va a correr en el puerto `3000`
- Posteriormente levantar en `frontend` que va a correr en el puerto `3001`

**Nice to have**

- Hacer todos los test del backend
- Cargar el tamaño adecuado para las imágenes
- Tener mejor claridad en las categorías para armar apropiadamente el `breacrumb`
