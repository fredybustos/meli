import { get } from "../services"
import { getProductsByQuery, parseProductDetail } from "../utils"

export const getProducts = async (req, res) => {
  try {
    let querySearch = ":query"
    if (req.query) {
      querySearch = req.query.search
    }
    const searchProducts = await get(`sites/MLA/search?q=${querySearch}`)

    const formatedSearchProducts = await getProductsByQuery(searchProducts)

    res.status(200).json(formatedSearchProducts)
  } catch (error) {
    res.status(404).send({ error: error.message })
  }
}

export const getProductById = async (req, res) => {
  const {
    params: { id },
  } = req
  const product = await get(`items/${id}`)

  const productDescription = await get(`items/${id}/description`)

  const formatedProduct = parseProductDetail(product, productDescription)
  res.status(200).json(formatedProduct)
}
