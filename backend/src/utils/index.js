export const getProductsByQuery = (externalResponse) => {
  if (!externalResponse?.results.length) return []
  const data = externalResponse.results.slice(0, 4)
  const categories = externalResponse.filters[0].values
  return parseResults(data, categories)
}

const parseResults = (dataToFormat, categories) => ({
  author: { name: "Fredy", lastname: "Bustos" },
  categories,
  items: dataToFormat.map((product) => parseProduct(product)),
})

const parseProduct = ({
  id,
  title,
  price,
  currency_id,
  thumbnail,
  condition,
  shipping,
  address,
}) => ({
  id,
  title,
  price: {
    amount: price,
    currency: currency_id,
    decimals: 0,
  },
  picture: thumbnail,
  condition,
  freeShipping: shipping.free_shipping,
  city: address.state_name,
})

export const parseProductDetail = (
  {
    id,
    title,
    price,
    currency_id,
    thumbnail,
    sold_quantity,
    condition,
    seller_address,
    category_id,
  },
  { plain_text }
) => ({
  title,
  item: {
    id,
  },
  price: {
    amount: price,
    currency: currency_id,
    decimals: 0,
  },
  picture: thumbnail,
  condition,
  soldQuantity: sold_quantity,
  city: seller_address.state.name,
  description: plain_text,
  category_id,
})
