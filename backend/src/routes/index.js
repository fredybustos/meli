import { Router } from "express"
import { getProducts, getProductById } from "../controllers"

const router = Router()

router.get("/api/items", getProducts)
router.get("/api/items/:id", getProductById)

export default router
