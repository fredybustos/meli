import express from "express"
import routes from "./routes"
import cors from "cors"

const app = express()

app.use(cors())
app.options("*", cors())
app.use(routes)
app.use(express.urlencoded({ extended: false }))
app.listen(3000)
