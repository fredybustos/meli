### Mercado Libre

Se construyeron 2 endpoints para consultar los productos y el detalle del mismo dado un id y se agregaron las categorías

#### Desarrollo

- [NodeJs](https://nodejs.org/es/download/package-manager) mayor a la versión 12

- [ExpressJs](https://expressjs.com/es/) Construir el API

- `npm install` Instalar dependencias necesarias para que corra el proyecto

- `npm run start` Esto levanta el servicio en el puerto `3000`

**Endpoints**

Los endpoints que se deben consultar son:

- `{{URL}}/api/items` Regresa la lista de productos dada una búsqueda, es necesario agregarle queryparams `?serach=iphone`
- `{{URL}}/api/items/{{id}}` Regresa la información del producto dado el id de mismo
